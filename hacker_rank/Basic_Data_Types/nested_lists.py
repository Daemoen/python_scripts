#!/usr/bin/env python3

"""
https://www.hackerrank.com/challenges/nested-list/problem?isFullScreen=true
"""

"""
Given the names and grades for each student in a class of N students, store them in a nested list
and print the name(s) of any student(s) having the second lowest grade.

Note: If there are multiple students with the second lowest grade, order their names alphabetically
and print each name on a new line.

### Example
records = [["chi", 20.0], ["beta", 50.0], ["alpha", 50.0]]
The ordered list of scores is [20.0, 50.0], so the second lowest score is 50.0.
There are two students with that score: ["beta", "alpha"]. Ordered alphabetically, the names are
printed as:

  alpha
  beta

### Input Format

The first line contains an integer, (N), the number of students.
The subsequent lines describe each student over 2 lines.

- The first line contains a student's name.
- The second line contains their grade.

### Constraints

2 <= N <= 5
There will always be one or more students having the second lowest grade.

### Output Format

Print the name(s) of any student(s) having the second lowest grade in. If there are multiple
students, order their names alphabetically and print each one on a new line.
"""

if __name__ == "__main__":
    # Outside the loop on purpose
    records = []
    students = []
    scores = []
    for _ in range(int(input())):
        # Yes, inside the loop on purpose, this way it resets on each iteration
        student_data = []
        name = input()
        score = float(input())

        student_data = [name, score]

        # Yeah, I know... its inefficient like this
        records.append(student_data)
        scores.append(score)

    # Yes.... using 1 because 0 index...
    low_score = sorted((set(scores)))[1]

    for student, score in records:
        if score == low_score:
            students.append(student)

    print(sorted(students))
