#!/usr/bin/env python3

"""
https://www.hackerrank.com/challenges/py-if-else/problem?isFullScreen=true
"""

"""
### Task
Given an integer, (n), perform the following conditional actions:

    If (n) is odd, print Weird
    If (n) is even and in the inclusive range of 2 to 5, print Not Weird
    If (n) is even and in the inclusive range of 6 to 20, print Weird
    If (n) is even and greater than 20, print Not Weird

###Input Format
A single line containing a positive integer, (n).

###Constraints
1 <= n <= 100

###Output Format
Print Weird if the number is weird. Otherwise, print Not Weird.
"""


def is_even(val):
    """Is the value even?  return true"""
    if val % 2 == 0:
        return True

    return False


def check_result(val):
    """Check the result to see which constraint it fits"""
    result = ""

    if val < 1 or val > 100:
        result = "Value exceeds constraints!"

    if (val >= 2 and val <= 5) or (val > 20):
        if is_even(val):
            result = "Not Weird"
        else:
            result = "Weird"

    if val >= 6 and val <= 20:
        result = "Weird"

    return result


if __name__ == "__main__":
    n = int(input("Enter an integer to check for weirdness: ").strip())
    print(check_result(n))
