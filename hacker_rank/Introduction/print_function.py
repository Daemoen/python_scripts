#!/usr/bin/env python3
"""
https://www.hackerrank.com/challenges/python-print/problem?isFullScreen=true
"""

"""
The included code stub will read an integer, (n), from STDIN.

Without using any string methods, try to print the following:

Note that "..." represents the consecutive values in between.

### Example
n = 5
Print the string 12345

### Input Format

The first line contains an integer (n)

### Constraints
1 <= n <= 150

### Output Format
Print the list of integers from 1 through (n) as a string, without spaces.
"""


def stringify(val):
    string = ""
    for seq in range(1, val + 1):
        string = string + str(seq)

    return string


if __name__ == "__main__":
    n = int(input("Enter an integer to sequence into a string: "))

    print(stringify(n))
