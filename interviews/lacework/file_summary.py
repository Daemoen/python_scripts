#!/usr/bin/env python3
import argparse
import hashlib
import json

# Requires python 3.12 for .walk()!
from pathlib import PurePath
from pathlib import Path

"""
The original scope of the interview was to just write a
duplicate file checker.  At the time, I struggled with it,
obviously.  As per usual, I went back and redefined the
task, expanding it, and came up with this version of the
request.  I actually really like this tool and may finish
it."""

"""
We wish to take a path, and iterate through all of the files
checking to see if there are any duplicate files.

Additionally, lets build a dataset that tells us:

1.) How many (likely) total duplicate files we have
2.) How many of each given filetype we have encountered

---
We wish to take multiple arguments:
1.) path(s)
2.) extension(s)

Lets default to '/' and all files.

Our return could be something like:

file_summary = {
    "count_ext_<EXT>": 12,
    "count_ext_<EXT>": 10,
    "duplicates": {
        likely: 2,
        dupe_results: {
            "a13cd21d": {
                "duplicates": 2,
                "files": ["/path/file3.png", "/path/file4.png"],
                "sizeof": 121315
            },
            "a13ff21d": {
                "duplicates": 2,
                "files": ["/path/file7.txt", "/path/file8.txt"],
                "sizeof": 131
            }
        },
    "file_data": [
        ["/path/file1.txt", "a12cd31d", 1],
        ["/path/file2.jpg", "a12dd31d", 1],
        ["/path/file3.png", "a13cd21d", 2],
        ["/path/file4.png", "a13cd21d", 2],
        ["/path/file5.txt", "a12fd31d", 1],
        ["/path/file6.jpeg", "a12cd31d", 1],
        ["/path/file7.txt", "a13ff21d", 2],
        ["/path/file8.txt", "a13ff21d", 2],
    ],
}

The above json gives us a list of *all* files, as well as their
checksum, and a summary of different extensions and how many of
that given extension we have seen, as well as an overall summary
of how many total (likely) duplicate files we have.  Duplicates likely
may not make sense at first, but it means 'how many unique checksums
have we seen duplicates of', not how many total files, as I feel this
is a more accurate representation of duplication.

Caveats:
    1.) We do not guarantee that a file is the actual type that
        matches it's given extension.  Ie: we trust the filename.
    2.) We understand that there are possibilities for collision.
        This is done more as an exercise, and can later be extended.

"""

""" reduce_opts() will handle the reduction and deduplication of paths
    and extensions before summarize() takes over and gives us the final
    result.
"""


def reduce_paths(check_paths):
    # This works, but I feel like it's pretty kludgy.  I'll try to refactor later.
    if r"/" in set(check_paths):
        reduced_paths = [r"/"]
    else:
        reduced_paths = sorted(set(check_paths), key=len)
        reversed_paths = sorted(set(check_paths), key=len, reverse=True)

        for full_path in reversed_paths:
            for root_path in reduced_paths:
                if PurePath(full_path).is_relative_to(root_path):
                    if full_path == root_path:
                        pass
                    else:
                        reduced_paths.remove(full_path)
                        break
    return reduced_paths


def reduce_exts(check_extensions):
    if r"*" in set(check_extensions):
        reduced_exts = [r"*"]
    else:
        reduced_exts = sorted(set(check_extensions))

    return reduced_exts
    #


""" summarize() will return the resultant json back to the user.  We'll
    use a dict, and output it as type json
"""


def summarize(reduced_paths, reduced_exts):
    file_summary = {
        "duplicates": {
            "likely": 0,
            "dupe_results": {},
        },
        "file_data": [],
    }

    for hashpath in reduced_paths:
        p = Path(hashpath)
        # Here, let's validate that the path we were given is actually a directory
        # if a person supplies a list of paths, but they aren't all real, this will
        # reduce the results further, which reduces the effort we spend checksummin.
        if not p.is_dir():
            pass
        else:
            for fileroot, _dirs, files in p.walk():
                for checkfile in files:
                    filepath = Path(str(fileroot) + "/" + checkfile)
                    if not filepath.is_file():
                        pass
                    else:
                        with open(filepath, "rb") as f:
                            digest = hashlib.file_digest(f, "sha512")
                            file_summary["file_data"].append(
                                [str(filepath), digest.hexdigest()]
                            )

    return json.dumps(file_summary)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="file_data.py",
        description="Gather data about files and check for potential duplicates",
    )
    parser.add_argument(
        "-e",
        default="*",
        help="Extension(s) you wish to gather information on.  Defaults to %(default)s",
        nargs="*",
        dest="check_extensions",
    )
    parser.add_argument(
        "-p",
        default="/",
        help="Directory path(s) for which we want to gather data.  Defaults to: %(default)s",
        nargs="*",
        dest="check_paths",
    )
    args = parser.parse_args()

    opts = vars(args)
    check_paths = opts["check_paths"]
    check_extensions = opts["check_extensions"]

    print(summarize(reduce_paths(check_paths), reduce_exts(check_extensions)))
