# Quantcast

## Overview

This script is a simple parser of log data. It allows a user to match on host (-h), and
query strings (-q). It will read in any number of files passed in to the -f argument, though
it cannot handle '\*'. You can however use shell expansion.

Some working examples:

`python3 parse_logs.py -f data.log -s '*' -q 'serve' 2>/dev/null`
`python3 parse_logs.py -f log1.log log2.log -s 'api-gateway-01.example.com' -q '*' 2>/dev/null`
`python3 parse_logs.py -f log*.log -s '*' -q 'serve' 'oad' 2>/dev/null`

_Note:_ Because of -h being used for 'help' in argparse, it had to change to source, which
is a bit awkward.
