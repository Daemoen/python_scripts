#!/usr/bin/env python3
import argparse
import json
import sys


"""
{'timestamp': '2023-01-01T23:10:18',
  'level': 'DEBUG',
  'message': 'Wilt thou serve me?',
  'host': 'host2.example.com'},
"""


def parse_args():
    parser = argparse.ArgumentParser(
        prog="parse_logs.py",
        description="Parse log files for information",
    )
    parser.add_argument(
        "-f",
        default="data.log",
        help="File(s) you wish to parse.  Defaults to %(default)s",
        nargs="*",
        dest="file_paths",
    )
    parser.add_argument(
        "-s",
        default="*",
        help="Source(s) (host(s)) to parse from the log file.  Defaults to: %(default)s",
        nargs="*",
        dest="check_sources",
    )
    parser.add_argument(
        "-q",
        default="*",
        help="Query (string(s)) to search for in message.  Defaults to: %(default)s",
        nargs="*",
        dest="check_queries",
    )
    args = parser.parse_args()

    opts = vars(args)

    # Use set to remove duplicates
    check_paths = set(opts["file_paths"])
    check_sources = set(opts["check_sources"])
    check_queries = set(opts["check_queries"])

    ## Deduplication via set
    parse_queries(check_queries, parse_sources(check_sources, load_file(check_paths)))


def load_file(check_paths):
    raw_merged_result = []
    for FP in check_paths:
        # We can't really handle wildcards.  You can use them at the shell expansion level, just not inside the script, at least not yet.
        if "*" in FP:
            print(
                "Error: Cannot handle wildcards as part of path at this time.",
                file=sys.stderr,
            )
            pass
        else:
            try:
                with open(FP, "r") as lines:
                    try:
                        ## I had attempted to use 'for line in lines if line not null', that didnt work, so I asked chatgpt how to correct it
                        ## chatgpt pointed out .strip() along with the advantages.
                        json_data = [json.loads(line) for line in lines if line.strip()]
                        raw_merged_result.append(json_data)
                    except json.decoder.JSONDecodeError as e:
                        pass
            except Exception as e:
                print(e)

    # Asked chatgpt for the best way to convert my list of list of dicts into a single list of dicts
    raw_merged_result = [item for sublist in raw_merged_result for item in sublist]
    return raw_merged_result


# Going to want to be able to filter on specific hosts
def parse_sources(check_sources, raw_merged_result):
    # check_sources should be a unique set, but it could also have a '*' in it, which means dont select any unique host at all, process them all
    sources_results = []
    if "*" in check_sources:
        sources_results = raw_merged_result
    else:
        for log_line in raw_merged_result:
            if any(check_source in log_line["host"] for check_source in check_sources):
                sources_results.append(log_line)

    return sources_results


# Going to want to reduct the result from hosts to the matches only.
def parse_queries(check_queries, sources_results):
    # query_results should be a unique set, but it could also have a '*' in it, which means dont select any unique host at all, process them all
    query_results = []
    if "*" in check_queries:
        query_results = sources_results
    else:
        for log_line in sources_results:
            if any(check_query in log_line["message"] for check_query in check_queries):
                query_results.append(log_line)

    print(json.dumps(query_results))


if __name__ == "__main__":
    parse_args()
